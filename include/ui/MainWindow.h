#pragma once

#include <exception>
#include <QtWidgets>

#include "ui_MainWindow.h"

#include "db/Connection.h"

namespace ui {

const int kStatusBarMsgTimeout = 3000;

class MainWindow : public QMainWindow {
  Q_OBJECT

 private:
  Ui::MainWindow form;

  db::Connection dbconn;

 public:
  MainWindow(QWidget *parent = nullptr);
  ~MainWindow();

 private:
  void setDbconnStatus(bool connected);
  void showDbconnError(std::exception &e);

 private slots:
  void dbconnOpenPbtClicked(bool checked = false);
  void dbconnClosePbtClicked(bool checked = false);
  void dbconnStatusPbtClicked(bool checked = false);

  void dbschemaSetPbtClicked(bool checked = false);
  void dbschemaCheckPbtClicked(bool checked = false);

  void peoplePbtClicked(bool checked = false);
  void peopleClearTxtPbtClicked(bool checked = false);
};

} // namespace ui
