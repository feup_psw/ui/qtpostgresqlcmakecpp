# 003_QtPostgreSQLCMake

Project example with both database connection
([PostgreSQL](https://www.postgresql.org/)) and Graphics User Interface (GUI),
where the latter is based on [Qt](https://www.qt.io/). Although
[Qt](https://www.qt.io/) is not open-source, this frameworks is used in
enterprise applications, is cross-platform (Windows, Linux, macOS, iOS, Android,
among other OS, i.e., Operating Systems), and supports both C++ and Python
programming languages. Also, [Qt](https://www.qt.io/) is available under both
open-source and commercial licenses (see more in this
[link](https://www.qt.io/download-open-source)). Therefore,
[Qt](https://www.qt.io/) is useful for any programmer interested in developing
applications with a GUI.

The main goals of this project are the following ones:
- configuration of the development tools including
  [PostgreSQL](https://www.postgresql.org/) and [Qt](https://www.qt.io/);
- build the project using CMake to demonstrate the development of a
  cross-platform application;
- compatibility with both Windows and Ubuntu operating systems.

**Version 1.0.0**

**With this version, it is possible to do:**

- Build project using CMake with [PostgreSQL](https://www.postgresql.org/) and
  [Qt](https://www.qt.io/) as dependencies of the project
- [Qt](https://www.qt.io/)-based main window for interacting with the user
- Connection to a [PostgreSQL](https://www.postgresql.org/) database
- Set schema of the database
- Get all people from the database

**The next version will add these features:**

No further developments are foreseen for the next version.

**Bugs identified in the current version:**

Nothing to be reported here in this version.

## Installation

### IDE / Text Editor

**Visual Studio Code (Ubuntu 20.04.5 LTS)**

See the configuration of a C/C++ development environment in
https://code.visualstudio.com/docs/languages/cpp.

1. Open a terminal and execute the following commands (install the building
   tools required for C++ applications):
   ```sh
   sudo apt update
   sudo apt dist-upgrade
   sudo apt install build-essential gdb
   sudo apt install cmake
   ```
2. Go to the Visual Studio Code [webpage](https://code.visualstudio.com/)
3. Install Visual Studio Code
   1. Download the `.deb` file
   2. Open a terminal and execute the following commands:
      ```sh
      cd ~/Downloads
      sudo apt install ./code_<version>_amd64.deb
      ```
4. Open Visual Studio Code
   1. Go to the extensions marketplace (File > Preferences > Extensions)
   1. Install
      [CMake](https://marketplace.visualstudio.com/items?itemName=twxs.cmake)
      and
      [C/C++ Extension Pack](https://marketplace.visualstudio.com/items?itemName=ms-vscode.cpptools-extension-pack)
   2. Configure the settings of Visual Studio Code
      1. Open Settings (File > Preferences > Settings)
      2. Search `cmake`
      3. Confirm that `Build directory` is `${workspaceFolder}/build`
      4. You can change other options for changing the compilation behavior
   3. Configure the IntelliSense settings of Visual Studio Code
      1. Open Command Pallete (Ctrl + Shift + P)
      2. Insert `C/C++: Edit Configurations (UI)`
      3. Edit the _Include path_ and add the following directories:
         - **Ubuntu 20.04.5 LTS**
           - `"${workspaceFolder}/include/**"`
           - `"/usr/include/**"`
           - `"${workspaceFolder}/build/<project name>_autogen/**"`
         - **Windows 11**
           - `"${workspaceFolder}/include/**"`
           - `"C:/Program Files/Microsoft Visual Studio/2022/Community/VC/Tools/MSVC/14.32.31326/include/**"`
           - `"${workspaceFolder}/build/<project name>_autogen/**"`
           - `"C:/Qt/6.2.4/msvc2019_64/include/**"`
           - `"C:/Program Files/PostgreSQL/15/include/**"`

**Visual Studio Community 2022 (Windows 11)**

1. Go to the Visual Studio [webpage](https://visualstudio.microsoft.com/)
2. Install Visual Studio Community 2022
   1. Download the Visual Studio Community isntaller (click on
      _Download Visual Studio_ in this
      [webpage](https://visualstudio.microsoft.com/vs/community/))
   2. Add the following workloads for installation:
      - Desktop development with C++
      - Game development with C++
      - Linux and embbeded development with C++
   3. Install Visual Studio Community 2022 while downloading
3. Go to the CMake [webpage](https://cmake.org/)
4. Install CMake
   1. Download the `x64` installer of the latest release available in the
      Downloads [webpage](https://cmake.org/download)
   2. Install CMake choosing to add CMake to the Windows' environment variable
      `Path` in the installation procedure

### libpq (PostgreSQL C Client Library)

See the official documentation of the
[libpq](https://www.postgresql.org/docs/current/libpq.html) library for
developing PostgreSQL-based applications.

**Ubuntu 20.04.5 LTS**

1. Open a new terminal (Ctrl + Alt + T)
2. Install the libraries and headers for PostgreSQL C development
   ```sh
   # Check for updates
   sudo apt update
   sudo apt dist-upgrade

   # Install package
   sudo apt install libpq-dev postgresql-server-dev-all
   ```

**Windows 11**

1. Go to [PosgreSQL Downloads](https://www.postgresql.org/download/) page
2. Select Windows operating system
3. Download the installer certified by EDB for all supported PostgreSQL versions
   1. Open the webpage
   2. Select latest version of PostgreSQL (v15.0) for `Windows x86_64`
4. Install PostgreSQL
   - Select all 4 componnents (pgAdmin 4 is not really needed unless you want a
     visualization of the database)
   - No additional software is needed to install through Stack Builder (if you
     were using Java or C#, you would probably want to install `pgJDBC` or
     `psqlODBC` database drivers, respectively)
5. Add PostgreSQL to the `Path` OS environment variable
   1. Search in Windows for `Edit environment variables`
   2. Choose `Path` variable in your _User variables_ and select
      _Edit_
   3. Select _New_ and add the PostgreSQL binaries folder to path
      (`C:\Program Files\PostgreSQL\15\bin`, make sure this path exists!)
   4. Select _Ok_ to save the updated `Path` variable

### Qt

1. Go to the _Qt for Open Source Development_
   [webpage](https://www.qt.io/download-open-source)
2. Download the Qt Online Installer
3. Install Qt
   - **Ubuntu 20.04.5 LTS**
     ```sh
     cd ~/Downloads
     chmod +x qt-unified-linux-x64-<version>-online.run
     ./qt-unified-linux-x64-<version>-online.run
     ```
   - **Windows 11**
     - Just run the `.exe` file download to your computer
   - **Common steps to both OS**
     1. Create a Qt account
     2. Choose _Custom installation_
     3. Select only the _LTS_ (Long-Term Support) category and click on _Filter_
     3. Select the following componnents:
        - Qt 6.2.4
          - MSVC 2019
          - Sources
          - Qt Quick 3D
          - Qt 5 Compatibility Module
          - Qt Shader Tools
          - Additional Libraries
          - Qt Debuck Information Files
          - Qt Quick Timeline
        - Developer and Designer Tools
          - Qt Creator
          - Debugging Tools for Windows
          - Qt Designer Studio
          - CMake
          - Ninja
     4. Install and wait for a long time (also, have some free space in your
        disk: in **Windows 11**, installation will use 16.14GB of disk space,
        considering the componnent items referred in step 3.)
4. If you are in **Ubuntu 20.04.5 LTS**, go to the
   [`CMakeLists.txt`](CMakeLists.txt) and change the `CMAKE_PREFIX_PATH`
   variable depending on where do you have Qt installed in your system
5. If you are in **Windows 11**, update the following environment variables
   - Create new variable `QT_DIR`
     - `C:\Qt\6.2.4\mingw81_64` (compiler gcc / g++ on MinGW x64)
     - `C:\Qt\6.2.4\msvc2019_64` (compiler of Microsoft / Visual Studio)
   - Add the following item to `PATH`: `%QT_DIR%\bin`
6. If you want to add new componnents to Qt or update / remove old ones
   - **Ubuntu 20.04.5 LTS**
     ```sh
     cd
     cd Qt
     ./MaintenanceTool
     ```
   - **Windows 11**
     1. Open the Windows Start menu
     2. Search for _Qt Maintenance Tool_
     3. Execute the program and update / remove componnets to your Qt
        installation

### VPN Connection

Source: https://www.up.pt/it/pt/servicos/redes-e-conetividade/vpn-eca13b99.

**Ubuntu 20.04.5 LTS**

1. Open Settings (Show All Applications > Settings)
2. Go to VPN connections (click on Network)
3. Add new VPN connection:
   1. Select Point-to-Point Tunneling Protocol (PPTP)
   2. Fill in the VPN parameters (Identity):
      - Name: `FEUP`
      - Gateway: `feup-vpn.up.pt`
      - User name + Password: your credentials (`...@fe.up.pt`)
        - Click on the symbol next to password > Select _Store the password for
          all users_
      - Advanced > Check _Use Point-to-Point encryption (MPPE)_

**Windows 11**

1. Install Check Point Capsule VPN
   1. Open Microsoft Store
   2. Search for Check Point Capsule VPN
   3. Install the application
2. Open Settings (Right-click Windows logo > Settings)
3. Go to VPN connections (Network & internet > VPN)
4. Add new VPN connection:
   - VPN provider: Check Point Capsule VPN
   - Connection name: `FEUP VPN`
   - Server name or address: `193.136.33.254`
   - User name + Password: your credentials (`...@fe.up.pt`)
   - Check _Remember my sign-in info_

## Usage

### VPN Connection

**Ubuntu 20.04.5 LTS**

1. Open Settings (Show All Applications > Settings)
2. Go to VPN connections (click on Network)
3. Connect to `FEUP VPN`

**Windows 11**

1. Open Settings (Right-click on the Windows logo > Settings)
2. Go to _Network & internet_ > VPN
3. Connect to `FEUP VPN`

### Database

See [documentation](doc/) for further details on how to create schemas, tables,
and populate the database.

### Compile & Run

**Ubuntu 20.04.5 LTS**

1. Open the project's folder in Visual Studio Code
   ```sh
   # Create a folder dedicated to project development
   cd
   mkdir dev
   cd dev

   # Clone the repository
   git clone https://gitlab.com/<link to project>
   cd <repository folder name>
   code .
   ```
2. Click `Yes` to setup the cmake and select `g++` compiler
3. Open `CMakeLists.txt` file and Ctrl+S (Output show open automatically and
   generate a `build/` folder in your workspace)
3. Open terminal (Terminal > New Terminal)
   ```sh
   cd build
   make
   ls
   ./<project name>
   ```

**Windows 11**

1. Open CMake (cmake-gui)
2. Configure the project:
   - Where is the source code:
     `C:/Users/sousa/Dev/<repository folder name>`
   - Where to build the binaries:
     `C:/Users/sousa/Dev/<repository folder name>/build`
3. Press Configure and Generate (current generator: Visual Studio 17 2022)
4. Click Open Project (will open automatically Visual Studio 2022)
5. Build the project
   1. Open Solution Explorer
   2. Righ-click on `ALL_BUILD` > Build
6. Run the executable
   1. Open Solution Explorer
   2. Righ-click on `<project name>` > Set as Startup Project
   3. Run (F5 | Ctrl + F5: with | without debug)

### Design UI

**Create new form**

1. Open Qt Designer
2. Create a new form
   1. File > New
   2. Select from the available templates/forms (e.g., Main Window or Widget)
3. Start editing the form through the Qt Designer interface
4. Save the file (Ctrl + S)
5. Compile again the project for generating the automoc files from the `ui` ones
   (for IntelliSense detect new or edited elements from the `.ui` class)

**Edit existent form**

1. Open Qt Designer
2. Open the `.ui` file that you want to change
   1. File > Open
   2. Select the `.ui` file
3. Start editing the form through the Qt Designer interface
4. Save the file (Ctrl + S)
5. Compile again the project for generating the automoc files from the `ui` ones
   (for IntelliSense detect new or edited elements from the `.ui` class)

## Support

Please contact Ricardo B. Sousa (rbs@fe.up.pt) if you have any questions.

## Authors

- Ricardo B. Sousa ([gitlab](https://gitlab.com/sousarbarb/),
  [github](https://github.com/sousarbarb/),
  [email](mailto:rbs@fe.up.pt))
