#include <iostream>

#include <QApplication>

#include "ui/MainWindow.h"

int main(int argc, char **argv) {
  QApplication app(argc, argv);

  ui::MainWindow w;
  w.show();

  return app.exec();
}
