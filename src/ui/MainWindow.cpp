#include "ui/MainWindow.h"

#include <string>
#include <sstream>

#include "db/Query.h"
#include "db/People.h"
#include "logic/People.h"

namespace ui {

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent) ,
    dbconn(db::kDbName, db::kDbHostIP, db::kDbUsername, db::kDbPassword) {
  form.setupUi(this);

  // Events configuration
  // - DB connection
  connect(form.dbconn_open_pbt, &QPushButton::clicked,
          this, &MainWindow::dbconnOpenPbtClicked);
  connect(form.dbconn_close_pbt, &QPushButton::clicked,
          this, &MainWindow::dbconnClosePbtClicked);
  connect(form.dbconn_status_pbt, &QPushButton::clicked,
          this, &MainWindow::dbconnStatusPbtClicked);
  // - DB schema
  connect(form.dbschema_set_pbt, &QPushButton::clicked,
          this, &MainWindow::dbschemaSetPbtClicked);
  connect(form.dbschema_check_pbt, &QPushButton::clicked,
          this, &MainWindow::dbschemaCheckPbtClicked);
  // - People
  connect(form.people_pbt, &QPushButton::clicked,
          this, &MainWindow::peoplePbtClicked);
  connect(form.people_clear_txt_pbt, &QPushButton::clicked,
          this, &MainWindow::peopleClearTxtPbtClicked);
}

MainWindow::~MainWindow() {
  // not necessary to close dbconn because close() is already called in the
  // Connection class destructor
}

void MainWindow::setDbconnStatus(bool connected) {
  form.dbconn_status_led->setStyleSheet(
    connected?  "QLineEdit{background-color:green}" :
                "QLineEdit{background-color:red}");
  form.dbconn_status_led->setText(connected? "OK" : "NOK");
}

void MainWindow::showDbconnError(std::exception &e) {
  form.statusbar->showMessage(e.what(), kStatusBarMsgTimeout);
  setDbconnStatus(false);
  dbconn.close();
}

void MainWindow::dbconnOpenPbtClicked(bool checked) {
  try {
    dbconn.connect();
  } catch (std::exception& e) {
    showDbconnError(e);
    return;
  }
  setDbconnStatus(true);
}

void MainWindow::dbconnClosePbtClicked(bool checked) {
  dbconn.close();
  setDbconnStatus(false);
}

void MainWindow::dbconnStatusPbtClicked(bool checked) {
  setDbconnStatus(dbconn.isGood());
}

void MainWindow::dbschemaSetPbtClicked(bool checked) {
  db::Query dbquery(dbconn);
  std::string schema = form.dbschema_set_led->text().toStdString();

  try {
    dbquery.setSchema( schema );
  } catch (std::exception& e) {
    form.statusbar->showMessage(e.what(), kStatusBarMsgTimeout);
    return;
  }

  form.statusbar->showMessage(("Schema set to " + schema).c_str(),
      kStatusBarMsgTimeout);
}

void MainWindow::dbschemaCheckPbtClicked(bool checked) {
  db::Query dbquery(dbconn);

  try {
    form.dbschema_check_led->setText( dbquery.showSearchPath().c_str() );
  } catch (std::exception& e) {
    form.statusbar->showMessage(e.what(), kStatusBarMsgTimeout);
    return;
  }
}

void MainWindow::peoplePbtClicked(bool checked) {
  db::People people_query(dbconn);

  // Get all people in the database
  std::stringstream output("");
  std::vector<logic::People> all_people;

  people_query.getAll(all_people);

  // Print all people saved in the database
  for (auto single_people : all_people) {
    output << single_people.name  << " "
           << single_people.email << " "
           << single_people.code  << std::endl;
  }

  form.people_ted->setText(output.str().c_str());
}

void MainWindow::peopleClearTxtPbtClicked(bool checked) {
  form.people_ted->setText("");
}

} // namespace ui
