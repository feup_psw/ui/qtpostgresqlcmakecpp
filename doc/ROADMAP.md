# Roadmap

## Current Product Backlog

No further developments are foreseen for the project.

## Version 1

- [x] Build project using CMake with [PostgreSQL](https://www.postgresql.org/)
      and [Qt](https://www.qt.io/) as dependencies of the project
- [x] [Qt](https://www.qt.io/)-based main window for interacting with the user
- [x] Connection to a [PostgreSQL](https://www.postgresql.org/) database
- [x] Set schema of the database
- [x] Get all people from the database
