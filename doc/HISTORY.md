# History

## Version 1

## Version 1.0

**Version 1.0.0** (2022/11/19)

- Get all people from the database

## Version 0

### Version 0.4

**Version 0.4.0** (2022/11/19)

- Set schema of the database

### Version 0.3

**Version 0.3.0** (2022/11/19)

- Connection to a [PostgreSQL](https://www.postgresql.org/) database

### Version 0.2

**Version 0.2.0** (2022/11/19)

- [Qt](https://www.qt.io/)-based main window for interacting with the user

### Version 0.1

**Version 0.1.0** (2022/11/19)

- Build project using CMake with [PostgreSQL](https://www.postgresql.org/) and
  [Qt](https://www.qt.io/) as dependencies of the project
